const urlUser = 'https://ajax.test-danit.com/api/json/users';
const urlPost = 'https://ajax.test-danit.com/api/json/posts';

class Card{
    constructor() {
        this.dataUser = [];
        this.dataPost = [];
    }
    async getUser(){
        fetch(urlUser)
            .then((response)=> response.json())
            .then((user) => {
                this.dataUser = user;
            })
    }

    async getPost () {
        fetch(urlPost)
            .then((response) => response.json())
            .then((post) => {
                this.dataPost = post;
                this.getRender()
            })
    }

    async getRender(){
        this.dataPost.forEach(card => {
            const div = document.createElement("div");
            div.classList.add("card");
            const h4 = document.createElement("h4");
            h4.classList.add("card-title");
            div.append(h4)
            const p1 = document.createElement("p");
            p1.classList.add("card-text");
            div.append(p1)

            this.dataUser.filter(item => {
                if (item.id === card.userId){
                    const p2 = document.createElement("p");
                    p2.classList.add("card_name_email")
                    p2.innerText = item.name + " " + item.email;
                    div.append(p2)
                }
            })
            h4.innerText = card.title
            p1.innerText = card.body
            document.body.append(div)
        })

    }

}

const allCards = new Card();
allCards.getUser();
allCards.getPost();
