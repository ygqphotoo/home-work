const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

let root = document.getElementById('root');
let list = document.createElement('ul');
list.style.listStyle = 'none';
root.appendChild(list);

for (let key in books) {
    if (books[key].author && books[key].name && books[key].price) {
        let item = document.createElement('li');
        item.innerHTML = books[key].author + ' ' + books[key].name + ' ' + books[key].price;
        list.appendChild(item);
    }
    if (books[key] !== books[key].author){
        try {
            throw new Error('Error author')
        }catch (err){
            console.log('Error author')
        }
    }
    if (books[key] !== books[key].name){
        try {
            throw new Error('Error name')
        }catch (err){
            console.log('Error name')
        }
    }
    if (books[key] !== books[key].price){
        try {
            throw new Error('Error price')
        }catch (err){
            console.log('Error price')
        }
    }
}
console.log(books);




