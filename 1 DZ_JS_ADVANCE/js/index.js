class Employee {
    constructor(name, age, solary) {
        this.name = name;
        this.age = age;
        this.solary = solary;
    }
    get name(){
        return this._name;
    }
    set name(value){
        return this._name = value;
    }
    get age(){
        return this._age;
    }
    set age(value){
        return this._age = value;
    }
    get solary(){
        return this._solary;
    }
    set solary(value){
        return this._solary = value;
    }
}
class Programmer extends Employee {
    constructor(name, age, solary, lang) {
        super(name, age, solary);
        this.lang = lang;
    }
    get solary() {
        return this._solary;
    }
    set solary(value) {
        return this._solary = value * 3;
    }
}

const object = new Employee('Genya', 25, 100000, 'eng');
const object2 = new Programmer('Uasya', 35, 100000, 'eng');

console.log(object);
console.log(object2);