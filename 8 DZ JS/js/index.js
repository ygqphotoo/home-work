"use strict";

const numberAria = document.querySelector('.input');

numberAria.addEventListener('focus', function (){
    numberAria.classList.add('focused');
    if (document.querySelector('p')) {
        document.querySelector('p').remove();
    numberAria.classList.remove('invalid');
    }
})
numberAria.addEventListener('blur', function (){
    const span = document.createElement('span');
    const button = document.createElement('button');
    button.addEventListener('click', () => removeElement(span))
    if (numberAria.value === ''){
        numberAria.classList.remove('focused')
    } else if (Math.sign(numberAria.value) === -1 ){
        numberAria.classList.remove('focused')
        numberAria.classList.add('invalid')
        numberAria.insertAdjacentHTML('beforebegin', '<p>Текущая цена: $</p>');
    }
        else {
        span.innerText = `Текущая цена ${numberAria.value}`;
        numberAria.classList.add('valid');
        numberAria.append(span)
        button.innerHTML = `X`;
        span.appendChild(button);
    }
    })



function removeElement(span){
    span.remove();
    numberAria.value = '';
}