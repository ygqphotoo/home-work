"use strict";

const hidden = document.getElementsByClassName('hidden');
const liList = Array.from(document.getElementsByClassName('tabs-title'));

const classHidden = (cls, length = cls.length, count = 0) => {
    for (let i = count; i < length; i++) {
        cls[i].style.display = 'none';
    }
};
classHidden(hidden);

function showContent() {
    const liList = Array.from(document.getElementsByClassName('tabs-title'));

    const dataNumber = this.getAttribute('data-number');

    const tabsContent = Array.from(document.getElementsByClassName('tabs-content-item'));

    if (this.classList.contains('active')) {
        return;
    }
    liList.forEach((value) => value.classList.remove('active'));
    this.classList.add('active');

    tabsContent.forEach((value) => value.style.display = 'none');

    const NumberContent = tabsContent.filter((items) => {
        return items.getAttribute('data-number') === dataNumber;
    });
    NumberContent[0].style.display = '';
}

liList.forEach((value) => value.addEventListener('click', showContent));