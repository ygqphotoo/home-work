"use strict";
function filterBy(array, type) {
    return array.reduce((newArray, currentItem) => {
        if (typeof currentItem != type) {
            newArray.push(currentItem);
        } return newArray
    }, []);
};
console.log(filterBy(['hello', 'world', 23, '23', null], "string"));
