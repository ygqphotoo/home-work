"use strict";

const hidden = document.getElementsByClassName('hidden');
const liList = Array.from(document.getElementsByClassName('service-nav-item'));

const tegA = Array.from(document.getElementsByTagName('a'));
tegA.forEach((value) => value.addEventListener('click', (e) => e.preventDefault()));

const classHidden = (cls, length = cls.length, count = 0) => {
    for (let i = count; i < length; i++) {
        cls[i].style.display = 'none';
    }
};
classHidden(hidden);

function showOurServices() {
    const liList = Array.from(document.getElementsByClassName('service-nav-item'));
    const dataNumber = this.getAttribute('data-number');
    const ourServicesMenu = Array.from(document.getElementsByClassName('service-information-content'));

    if (this.classList.contains('active')) {
        return;
    }
    liList.forEach((value) => value.classList.remove('active'));
    this.classList.add('active');

    ourServicesMenu.forEach((value) => value.style.display = 'none');

    const NumberOurServices = ourServicesMenu.filter((items) => {
        return items.getAttribute('data-number') === dataNumber;
    });
    NumberOurServices[0].style.display = '';
}

liList.forEach((value) => value.addEventListener('click', showOurServices));


