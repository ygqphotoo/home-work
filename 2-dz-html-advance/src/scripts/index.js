"use strict"

function burgerMenu (selector) {
    let burgerBtn = $(selector);
    let button = burgerBtn.find('.burger-menu-button');
    let overlay = burgerBtn.find('.burger-menu_overlay')

    button.on('click', (e) => {
        e.preventDefault();
        toggleMenu();
    });

    overlay.on('click', () => {
        toggleMenu()
    });

    function toggleMenu(){
        $('.menu-list').toggle()

        // const menuList = $('.menu-list')
        //
        // if (menuList.css('display') === 'none') {
        //     menuList.css('display', 'flex')
        // } else {
        //     menuList.css('display', 'none')
        // }

        burgerBtn.toggleClass('burger-menu_active');
        if(burgerBtn.hasClass('burger-menu_active')){
            $('body').css('overflow','hidden');
        } else {
           $('body').css('overflow','visible');
        }
    }
}
// burgerMenu('.burger-menu');

if ($('.burger-menu-button').css('display') === 'none') {
    $('.menu-list').css('display', 'flex')
} else {
    burgerMenu('.burger-menu');
}