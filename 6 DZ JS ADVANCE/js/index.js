const getIp = "https://api.ipify.org/?format=json";
const getAdr = "http://ip-api.com";

const btnId = document.querySelector('#btn-id');

btnId.addEventListener("click", async evt => {
    evt.preventDefault();
    const ip = await ipFind();
    await adrFind(ip)
})

async function ipFind (){
    const ipPromise = await fetch(getIp);
    const ipJson = await ipPromise.json()
    btnId.insertAdjacentHTML('afterend', " " + ipJson.ip)
    return ipJson.ip;
}
async function adrFind (ip){
    const adrPromise = await fetch(`${getAdr}/json/${ip}?fields=status,message,continent,continentCode,country,countryCode,region,regionName,city,zip,lat,lon,timezone,isp,org,as,query`);
    const adrJson = await adrPromise.json();
    btnId.insertAdjacentHTML("afterend",  " " + "( " + adrJson.continent + " " + adrJson.country + " " + adrJson.city + " " + adrJson.region + " )")
}

