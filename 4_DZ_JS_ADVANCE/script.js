const urlFilms = 'https://ajax.test-danit.com/api/swapi/films/';

function showFilms () {
    fetch(urlFilms)
        .then((response) => response.json())
        .then((data) => {
            data.forEach((filmsName) => {
                const promisedLinks = filmsName.characters.map((result) => {
                    return fetch(result)
                        .then(res => res.json())
                })
                Promise.all(promisedLinks)
                    .then((value) => {
                        value.forEach((chars) => {
                            document.body.insertAdjacentHTML("beforeend", `<h2>${filmsName.id} film ) ${chars.name}</h2>`,);
                        });
                        document.body.insertAdjacentHTML("beforebegin", `<h3>${filmsName.name}, ${filmsName.id}</h3> ***  ${filmsName.openingCrawl} `,);
                    })
            })
        })
}

showFilms()